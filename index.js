/**
 * Ecrire un algorithme qui demande à l’utilisateur
 *  un nombre compris entre 1 et 3 
 * jusqu’à ce que la réponse convienne.
 */

/**
 * Langage naturel:
 * 
 * Algorithme exo1
 * 
 * début
 * 
 * Afficher("Choisissez un nombre entre 1 et 3")
 * Saisir(unNombre)
 * 
 * Tant que unNombre est <=0 et unNombre >=4
 * 
 * Afficher("Erreur: veuillez entrer un nombre entre 1 et 3")
 * Saisir(unNombre)
 * 
 * fTant que
 * 
 * Retourner "Ok, le numéro est validé !"
 * 
 * fin
 */

function exo1() {
    let input = parseIntfindUniqNumber([0, 0, 0.55, 0, 0]) === 0.55
        (prompt("Choisissez un nombre entre 1 et 3: "));
    while (input <= 0 || input >= 4) {
        input = parseInt(prompt("Erreur: veuillez entrer un nombre entre 1 et 3"))
    }
    return "Ok, le numéro est validé !"
}

/**
 * Ecrire un algorithme qui demande un nombre
 * compris entre 10 et 20,
 * jusqu’à ce que la réponse convienne.
 *  En cas de réponse supérieure à 20,
 *  on fera apparaître un message :
 *  « Plus petit ! », et inversement,
 *  « Plus grand ! » si le nombre est inférieur à 10.
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo2
 * 
 * début
 * 
 * Afficher("Veuillez choisir un nombre entre 10 et 20")
 * Saisir(unNombre)
 * 
 * Tant que unNombre est <10 et unNombre >20
 * 
 * SI unNombre > 20
 * 
 * Afficher("Plus petit !")
 * Saisir(unNombre)
 * 
 * sinon SI unNombre < 10
 * 
 * Afficher("Plus grand !") 
 * Saisir(unNombre)
 * 
 * fsi
 * 
 * fTant que
 * 
 * Retourner "Ok, le numéro est validé !"
 * 
 * fin
 */
function exo2() {
    let input = parseInt(prompt("Veuillez choisir un nombre entre 10 et 20"))
    while (input < 10 || input > 20) {
        if (input > 20) {
            input = parseInt(prompt("Plus petit !"))
        } else if (input < 10) {
            input = parseInt(prompt("Plus grand !"))
        }
    }
    return "Ok, c'est validé !"
}

/**
 * Ecrire un algorithme qui demande 
 * un nombre de départ, et qui ensuite
 *  affiche les dix nombres suivants.
 *  Par exemple, si l'utilisateur entre le nombre 17,
 * le programme affichera les nombres de 18 à 27.
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo3
 * 
 * début
 * 
 * Afficher("Veuillez choisir un nombre de départ")
 * Saisir(unNombre)
 * 
 * result <- unNombre
 * 
 * 
 * Tant que unNombre != result+10
 * 
 * input <- input+1
 * Afficher(unNombre)
 * 
 * fTant que
 * 
 * retourner unNombre
 * 
 * fin
 */

function exo3() {
    let input = parseInt(prompt("Veuillez choisir un nombre de départ"))
    let result = input
    while (input !== result + 10) {
        input++
        console.log(input)
    }
    return input
}

/**
 * Réécrire l'algorithme précédent,
 *  en utilisant cette fois l'instruction
 *  Pour (Si ce n'est pas déjà le cas)
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo4
 * 
 * début
 * Afficher("Veuillez choisir un nombre de départ")
 * Saisir(unNombre)
 * 
 * Pour i allant de unNombre à unNombre != i+10 avec un pas de 1
 * Faire Afficher(unNombre+1)
 * 
 * fPour
 * 
 * fin
 */
function exo4() {
    let input = parseInt(prompt("Veuillez choisir un nombre de départ"))
    for (let i = input; input !== i + 10; input++) {
        console.log(input + 1)
    }
}
/**
 * Ecrire un algorithme qui demande 
 * un nombre de départ,
 *  et qui ensuite écrit la table de multiplication
 *  de ce nombre, présentée comme suit
 *  (cas où l'utilisateur entre le nombre 7)
 *  : Table de 7 : 7 x 1 = 7 7 x 2 = 14 7 x 3 = 21 ...
 *  7 x 10 = 70
 */

/**
 * Langage Naturel:
 * 
 * début
 * 
 * Afficher("Choisissez une valeur de départ")
 * Saisir(unNombre)
 * 
 * multiplier <- 1
 * 
 * result <- unNombre
 * 
 * Tant que result != unNombre*10
 * 
 * result <- unNombre * multiplier
 * Afficher("Table de unNombre: unNombre x multiplier = result")
 * multiplier <- multiplier+1
 * 
 * fTant que 
 * 
 * fin
 */
function exo5() {
    let input = parseInt(prompt("Choisissez une valeur de départ"))
    let multiplier = 1
    let result = input
    while (result !== input * 10) {
        result = input * multiplier
        console.log("Table de " + input + ":" + input + "x" + multiplier + "=" + result)
        multiplier++
    }
}

/**
 * Ecrivez un algorithme calculant 
 * la somme des valeurs d’un tableau 
 * (on suppose que le tableau a été préalablement saisi).
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo6
 * 
 * début
 * 
 * array <- [1,2,3,4]
 * sum <- 0
 * 
 * Pour i allant de 0 à i < array.length avec un pas de 1
 * 
 * sum <- sum+array[i]
 * 
 * fPour
 * 
 * Retourner sum
 * 
 * fin
 * 
 */
function exo6() {
    const array = [1, 2, 3, 4];
    let sum = 0;

    for (let i = 0; i < array.length; i++) {
        sum += array[i];
    }
    return sum
}

/**
 * Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur,
 *  et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres
 *  : Entrez le nombre numéro 1 : 12 
 * Entrez le nombre numéro 2 : 14 etc.
 *  Entrez le nombre numéro 20 : 6
 *  Le plus grand de ces nombres est : 14
 *  Modifiez ensuite l’algorithme pour que le programme affiche de surcroît
 *  en quelle position avait été saisie ce nombre : C’était le nombre numéro 2
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo7
 * 
 * début
 * 
 * maximum <- 0
 * indice <- O
 * 
 * Pour i allant de 1 à i<=20 avec un pas de 1
 * 
 * Afficher("Entrez le numéro <i> : ")
 * Saisir(unNombre)
 * 
 * Si i === 1
 * maximum <- input
 * indice <- i
 * 
 * sinon
 * 
 * Si input > maximum
 * maximum = input
 * indice = i
 * fsi
 * 
 * fsi
 * 
 * fPour
 * 
 * fin
 * 
 */
function exo7() {
    let maximum = 0
    let indice = 0
    for (i = 1; i <= 20; i++) {
        let input = parseInt(prompt("Entrez le numéro " + String(i) + " : "))
        if (i === 1) {
            maximum = input
            indice = i
        } else {
            if (input > maximum) {
                maximum = input
                indice = i
            }
        }
    }
    console.log("Le plus grand de ces nombres est: " + maximum)
    console.log("C'était le numéro" + indice)
}
/**
 * Réécrire l’algorithme précédent, mais cette fois-ci on ne connaît pas d’avance 
 * combien l’utilisateur souhaite saisir de nombres. 
 * La saisie des nombres s’arrête lorsque l’utilisateur entre un zéro.
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo8
 * 
 * début
 * 
 * maximum <- 0
 * indice <- O
 * 
 * i <- 1
 * ifini <- true
 * 
 * Tant que infini
 * 
 * Afficher("Entrez le numéro <i> : ")
 * Saisir(unNombre)
 * 
 * Si unNombre === 0
 * 
 * infini <- false
 * 
 * sinon
 * 
 * Si i === 1
 * 
 * maximum <- unNombre
 * indice <- i
 * 
 * sinon
 * 
 * Si unNombre > maximum
 * maximum <- unNombre
 * indice <- i
 * 
 * fsi
 * 
 * fsi
 * 
 * i <- i+1
 * 
 * fTant que
 * 
 * fin
 */
function exo8() {
    let maximum = 0
    let indice = 0

    let i = 1
    let infini = true

    while (infini) {
        let input = parseInt(prompt("Entrez le numéro " + String(i) + " : "))
        if (input === 0) {
            infini = false
        } else {
            if (i === 1) {
                maximum = input
                indice = i
            } else {
                if (input > maximum) {
                    maximum = input
                    indice = i
                }
            }
            i += 1
        }
    }
    console.log("Le plus grand de ces nombres est: " + maximum)
    console.log("C'était le numéro" + indice)
}

/**
 * Lire la suite des prix 
 * (en euros entiers et terminée par zéro)
 *  des achats d’un client. 
 * Calculer la somme qu’il doit,
 * lire la somme qu’il paye, et
 * simuler la remise de la monnaie en affichant 
 * les textes "10 Euros", "5 Euros" et "1 Euro"
 *  autant de fois qu’il y a de coupures de chaque 
 * sorte à rendre.
 */

/**
 * Langage Naturel:
 * 
 * Algorithme exo9
 * 
 * début
 * 
 * somme_due <- 0
 * somme_pay <- 0
 * nombre <- 1
 * remise <- 0
 * dix <- 0
 * cinq <- 0
 * un <- 0
 * 
 * Tant que nombre !=== 0
 * 
 * Afficher("Saisir un nombre (non null), saisir 0 pour arrêter la saisie")
 * Saisir(nombre)
 * somme_due <- somme_due+nombre
 * 
 * fTant que
 * 
 * Afficher("Vous devez <somme_due> euros")
 * Afficher("Indiques le montant que vous payez: ")
 * Saisir(somme_pay)
 * remise <- somme_pay - somme_due
 * remise_init <- remise
 * 
 * Tant que remise > 10
 * 
 * remise <- remise-10
 * dix <- dix+1
 * 
 * fTant que
 * 
 * Tant que remise > 5
 * 
 * remise <- remise-5
 * cinq <- cinq+1
 * 
 * fTant que
 * 
 * Tant que remise > 0
 * 
 * remise <- remise-1
 * un <- un+1
 * 
 * fTant que
 * 
 * Retourner("Votre remise est de: <remise_init> euros,
 *  vous recevez: <dix> billets de 10e <cinq> billets de 5e
 * <un> pièces de 1e")
 * 
 * fin
 */
function exo9() {
    let nombre = 1
    let somme_due,somme_pay,remise,dix,cinq,un=0
    while (nombre !== 0) {
        nombre = Number(prompt("Saisir un nombre (non null), saisir 0 pour arrêter la saisie"))
        somme_due += nombre
    }
    console.log("vous devez: " + somme_due + "euros")
    somme_pay = Number(prompt("indiquez le montant que vous payez: "))
    remise = somme_pay - somme_due
    let remise_init = remise
    while (remise > 10) {
        remise -= 10
        dix++
    }
    while (remise > 5) {
        remise -= 5
        cinq++
    }
    while (remise > 0) {
        remise -= 1
        un++
    }
    return "votre remise est de: " + remise_init + "euros, vous recevez: \n" + dix + " billets de 10e\n" + cinq + " billets de 5e\n" + un + " pièces de 1e"
}